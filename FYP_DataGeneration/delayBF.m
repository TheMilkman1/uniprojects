% This function implements and delay and sum beamformer and outputs the
% elevation(psi) and azimuth(theta) angles. The input signal is multiplied
% by a time delay vector, when the maximum of this product corresponds to
% the direction of arrival (with left right ambiguity).
% http://www.ecs.umass.edu/ece/sdp/sdp14/team15/assets/Team15FinalMDRReport.pdf
function [theta,psi] = delayBF(signal,TS,c,d,n)

x_sum = 0;
x_prev = 0;
theta = 0;
psi = 0;
%p_a = zeros(length(180));
%p_z = zeros(length(180));
coder.varsize('x_sum');
coder.varsize('x_prev');
zmax = 0;


for alpha = 1:180
   
    for i = 1:4
        % Time delay for wave front arriving from alpha direction. 
        tau = round(((i-1)*d*cosd(alpha)/c)/TS);  
        x = circshift(signal(:,i),tau);
        x_sum = x + x_sum;
    end
    
    z = max(abs(x_sum));
   
    if  all(z > x_prev)
       theta = alpha;
       zmax = z;
    end
    
    p_a(alpha)=alpha;
    p_z(alpha)=z;    

    x_sum = 0;
    x_prev = zmax;

end
% figure(2)
% plot(p_a,p_z)
% xlabel('Predicted Angle (deg)');
% ylabel('Gain (-)');
% title('Beamformer Output');
% pause(eps)
% figure(3)
% plot(signal)
end

    