#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mex.h"
#define DEG2RAD 0.0174532925199432

// Function Prototypes 
int LUT_tau(int alpha, int nSig);
double * shiftARR(double sig[], int tau, int size);
int delayBF(double *S1,double *S2,double *S3,double *S4);


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *inMatrix1;               // 1xN input matrix 
    double *inMatrix2;               // 1xN input matrix 
    double *inMatrix3;               // 1xN input matrix 
    double *inMatrix4;               // 1xN input matrix 

    inMatrix1 = mxGetPr(prhs[0]);
    inMatrix2 = mxGetPr(prhs[1]);
    inMatrix3 = mxGetPr(prhs[2]);
    inMatrix4 = mxGetPr(prhs[3]);

    /*  call the C subroutine */
    plhs[0] = mxCreateDoubleScalar(delayBF(inMatrix1,inMatrix2,inMatrix3,inMatrix4));

}
int delayBF(double *S1,double *S2,double *S3,double *S4)
{
	
// int size = sizeof(S1)/sizeof(S1[1]);  // size irrelevent of data type
    int size = 100001;   // Need a better way
// Initilise varibles
	double x_sum[size];
	double x_prev[size];
	int theta = 0;
	int tau1 = 0;
	double tempmax = 0;
    double tempmax_prev = 0;
    int alpha;
    int i,j,k,m;
    
//             mexPrintf("\nsig2 before shift");
//         for(m = 0; m<20; m++)
//         {
//            printf(" %f ", S2[m]);
//         }


	for (alpha=0; alpha < 180; alpha++) 
    {	
        int tau2 = LUT_tau(alpha, 1); 
        int tau3 = LUT_tau(alpha, 2); 
        int tau4 = LUT_tau(alpha, 3);
        
        
        //mexPrintf("alpha:%d tau2: %d tau3: %d tau4: %d\n", alpha, tau2, tau3, tau4);

        // Apply timeshift	
        S2 = shiftARR(S2, tau2, size);     
        S3 = shiftARR(S3, tau3, size);
        S4 = shiftARR(S4, tau4, size);
        
        for (m=0; m<size; m++){
            x_sum[m] = 0; // Init. on the run
            x_sum[m] = S1[m] + S2[m] + S3[m] + S4[m];
        }
        

//             printf("Element %d   S1: %lf S2: %lf S3: %lf S4: %lf Sum: %lf \n", alpha, S1[m], S2[m], S3[m], S4[m], x_sum[m]);
        
        
        //mexPrintf("\nsig2 after shift of:%d",tau2);
        
//         for(m = 0; m<30; m++)
//         {
//            printf(" %f ", S2[m]);
//         }
        
        
        // Shift back - help me
        S2 = shiftARR(S2, -tau2, size);
        S3 = shiftARR(S3, -tau3, size);
        S4 = shiftARR(S4, -tau4, size);
        

                
		for (j=1; j<size; j++) // finding the max value of x_sum aka the angle of arrival
		{
		    if (abs(x_sum[j]) > tempmax){
		      	tempmax = abs(x_sum[j]);
                //printf("tempmax= %lf  @ %d\n",tempmax,alpha);
            }   
        }    
        //printf("temp max %lf  \n", tempmax);
		// test and see if 'tempmax' is bigger than everything in array 'x_prev'
		
		if (tempmax > tempmax_prev) 
        {
			theta = alpha;
			tempmax_prev = tempmax;
		}
	}
    return theta+1;
}


double * shiftARR(double sig[], int tau, int size)
{
	if (tau>0) {
		int j;
		for (j=0; j<tau; j++) 
        {
            int i;
			for (i=0; i<(size -1); i++)
			{
				double temp = sig[size-1];
				sig[size-1] = sig[i];
				sig[i] = temp;
			}
		}	
	}	
	if (tau<0) {
        int j;
		for (j=0; j<abs(tau); j++) 
        {
			int i;
			double temp = sig[0];
		    for(i=0; i<size-1; i++) 
            {
		        sig[i] = sig[i+1];
		    }
		    sig[i] = temp;		
		}
	}	
	//if (tau == 0) 
		//mexPrintf("No Shift \n");
	return sig;
}

// Look up table -  Time delays/shifts
int LUT_tau(int alpha, int nSig)
{ 
    // 	** Parameters for this version **
    // 	double spacing = 0.02;       // Hydrophone separation (m)
    // 	int c = 1500;            // Speed of sound underwater (m/s)
    // 	double Ts = 1*10e-6;           // Sample time (s)
    int LUT_tau[180][4] =
	{{1,13,	27,	40},
	{2,	13,	27,	40},
	{3,	13,	27,	40},
	{4,	13,	27,	40},
	{5,	13,	27,	40},
	{6,	13,	27,	40},
	{7,	13,	26,	40},
	{8,	13,	26,	40},
	{9,	13,	26,	40},
	{10, 13,26,	39},
	{11, 13,26,	39},
	{12,13,	26,	39},
	{13,13,	26,	39},
	{14,13,	26,	39},
	{15,13,	26,	39},
	{16,13,	26,	38},
	{17,13,	26,	38},
	{18,13,	25,	38},
	{19,13,	25,	38},
	{20,13,	25,	38},
	{21,12,	25,	37},
	{22,12,	25,	37},
	{23,12,	25,	37},
	{24,12,	24,	37},
	{25,12,	24,	36},
	{26,12,	24,	36},
	{27,12,	24,	36},
	{28,12,	24,	35},
	{29,12,	23,	35},
	{30,12,	23,	35},
	{31,11,	23,	34},
	{32,11,	23,	34},
	{33,11,	22,	34},
	{34,11,	22,	33},
	{35,11,	22,	33},
	{36,11,	22,	32},
	{37,11,	21,	32},
	{38,11,	21,	32},
	{39,10,	21,	31},
	{40,10,	20,	31},
	{41,10,	20,	30},
	{42,10,	20,	30},
	{43,10,	20,	29},
	{44,10,	19,	29},
	{45,9,	19,	28},
	{46,9,	19,	28},
	{47,9,	18,	27},
	{48,9,	18,	27},
	{49,9,	17,	26},
	{50,9,	17,	26},
	{51,8,	17,	25},
	{52,8,	16,	25},
	{53,8,	16,	24},
	{54,8,	16,	24},
	{55,8,	15,	23},
	{56,7,	15,	22},
	{57,7,	15,	22},
	{58,7,	14,	21},
	{59,7,	14,	21},
	{60,7,	13,	20},
	{61,6,	13,	19},
	{62,6,	13,	19},
	{63,6,	12,	18},
	{64,6,	12,	18},
	{65,6,	11,	17},
	{66,5,	11,	16},
	{67,5,	10,	16},
	{68,5,	10,	15},
	{69,5,	10,	14},
	{70,5,	9,	14},
	{71,4,	9,	13},
	{72,4,	8,	12},
	{73,4,	8,	12},
	{74,4,	7,	11},
	{75,3,	7,	10},
	{76,3,	6,	10},
	{77,3,	6,	9},
	{78,3,	6,	8},
	{79,3,	5,	8},
	{80,2,	5,	7},
	{81,2,	4,	6},
	{82,2,	4,	6},
	{83,2,	3,	5},
	{84,1,	3,	4},
	{85,1,	2,	3},
	{86,1,	2,	3},
	{87,1,	1,	2},
	{88,0,	1,	1},
	{89,0,	0,	1},
	{90,0,	0,	0},
	{91,0,	0,	-1},
	{92,0,	-1,	-1},
	{93,-1,	-1,	-2},
	{94,-1,	-2,	-3},
	{95,-1,	-2,	-3},
	{96,-1,	-3,	-4},
	{97,-2,	-3,	-5},
	{98,-2,	-4,	-6},
	{99,-2,	-4,	-6},
	{100,-2,	-5,	-7},
	{101,-3,	-5,	-8},
	{102,-3,	-6,	-8},
	{103,-3,	-6,	-9},
	{104,-3,	-6,	-10},
	{105,-3,	-7,	-10},
	{106,-4,	-7,	-11},
	{107,-4,	-8,	-12},
	{108,-4,	-8,	-12},
	{109,-4,	-9,	-13},
	{110,-5,	-9,	-14},
	{111,-5,	-10,	-14},
	{112,-5,	-10,	-15},
	{113,-5,	-10,	-16},
	{114,-5,	-11,	-16},
	{115,-6,	-11,	-17},
	{116,-6,	-12,	-18},
	{117,-6,	-12,	-18},
	{118,-6,	-13,	-19},
	{119,-6,	-13,	-19},
	{120,-7,	-13,	-20},
	{121,-7,	-14,	-21},
	{122,-7,	-14,	-21},
	{123,-7,	-15,	-22},
	{124,-7,	-15,	-22},
	{125,-8,	-15,	-23},
	{126,-8,	-16,	-24},
	{127,-8,	-16,	-24},
	{128,-8,	-16,	-25},
	{129,-8,	-17,	-25},
	{130,-9,	-17,	-26},
	{131,-9,	-17,	-26},
	{132,-9,	-18,	-27},
	{133,-9,	-18,	-27},
	{134,-9,	-19,	-28},
	{135,-9,	-19,	-28},
	{136,-10,	-19,	-29},
	{137,-10,	-20,	-29},
	{138,-10,	-20,	-30},
	{139,-10,	-20,	-30},
	{140,-10,	-20,	-31},
	{141,-10,	-21,	-31},
	{142,-11,	-21,	-32},
	{143,-11,	-21,	-32},
	{144,-11,	-22,	-32},
	{145,-11,	-22,	-33},
	{146,-11,	-22,	-33},
	{147,-11,	-22,	-34},
	{148,-11,	-23,	-34},
	{149,-11,	-23,	-34},
	{150,-12,	-23,	-35},
	{151,-12,	-23,	-35},
	{152,-12,	-24,	-35},
	{153,-12,	-24,	-36},
	{154,-12,	-24,	-36},
	{155,-12,	-24,	-36},
	{156,-12,	-24,	-37},
	{157,-12,	-25,	-37},
	{158,-12,	-25,	-37},
	{159,-12,	-25,	-37},
	{160,-13,	-25,	-38},
	{161,-13,	-25,	-38},
	{162,-13,	-25,	-38},
	{163,-13,	-26,	-38},
	{164,-13,	-26,	-38},
	{165,-13,	-26,	-39},
	{166,-13,	-26,	-39},
	{167,-13,	-26,	-39},
	{168,-13,	-26,	-39},
	{169,-13,	-26,	-39},
	{170,-13,	-26,	-39},
	{171,-13,	-26,	-40},
	{172,-13,	-26,	-40},
	{173,-13,	-26,	-40},
	{174,-13,	-27,	-40},
	{175,-13,	-27,	-40},
	{176,-13,	-27,	-40},
	{177,-13,	-27,	-40},
	{178,-13,	-27,	-40},
	{179,-13,   -27,	-40},
	{180,-13,	-27,	-40}};
    
    return LUT_tau[alpha][nSig];
}