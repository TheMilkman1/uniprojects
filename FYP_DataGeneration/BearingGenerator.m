% Initilise
clear all
clc
clf

%mex BeamForm3.c  % Let's get mexy

% Approach trajectory
n = 201;                   % Number of points on approach
xo = linspace(-55, 0, n);  % Defining approach
yo = 20+ -5*sin(xo*.08);    % PF Approach
%yo = linspace(40, 10, n);  % UKF Approach
t = linspace(0, 100,n-1);

%% Parameters
hdist = 0.018;             % Distance between hydrophones in the array (m)

c = 1500;                       % Sound Speed (approx) (m/s)
TS = 1e-6 ;                     % Time Step (???) (sec)

SourceEntry = [10 20 0]';  % Define entry gate
SourceExit =  [10 30 0]';  % Define exit gate

%% Loopy loop

for k = 1:n-1
    % Source Signal Construction 
    %tic
    Cent = [xo(k) yo(k) 0]';  % Centroid of hydrophone array
    psi = atan2d(yo(k+1)-yo(k),xo(k+1)-xo(k));                % WAM-V Orientation;
    %t1 = toc
    %tic
    [S1, S2, S3, S4, tt, H] = SigGen(Cent, psi, hdist,SourceEntry);
    % [S1, S2, S3, S4, tt,H] = SigGen(Cent, psi, hdist,SourceExit);
    signal = [S1 S2 S3 S4];
    %t2 = toc
    %tic
    % Beamformer
    [Theta_Beam(k),~] = delayBF(signal,TS,c,hdist,4);
    %t3 = toc
    %tic
    % TDOA
    Theta_TDOA(k) = TDOA(S1, S4, hdist ,c,TS);
    %t4 = toc
    %tic
    %T_mex(k) = BeamForm3(S1, S2, S3, S4);
    %t5 = toc
    Theta_ref(k) = (atan2d(SourceEntry(2)-Cent(2),SourceEntry(1)-Cent(1))-psi);

end

%% Plotting
figure(1)
Theta_ref = -Theta_ref;
plot(t,Theta_TDOA,t,Theta_Beam,'r',t,Theta_ref)
legend('TDOA','Beamformer', 'Mexed Beamformer')
grid on






% 
% subplot(2,1,1)
% plot(tt,S1,tt,S2,tt,S3,tt,S4)
% legend('S1','S2','S3','S4')
% title ('Recieved Signals','fontsize',14)
% xlabel ('Time(s)','fontsize',14)
% ylabel ('Hydrophone Respnse (V)','fontsize',14)
% % 
% % 
% subplot(2,1,2)
% hold on
% scatter(SourceEntry(1),SourceEntry(2),'x')
% % scatter(h1(1),h1(2))
% % scatter(h2(1),h2(2))
% % scatter(h3(1),h3(2))
% % scatter(h4(1),h4(2))
% legend('source','h1','h2','h3','h4','trajectory')
% xlabel('Length(m)','fontsize',14)
% ylabel('Width(m)','fontsize',14)
% title('Competition Field','fontsize',14)
% axis([-30 42.5 0 42.5])
% grid on
% plot(x,y,':xr')
% legend('source','h1','h2','h3','h4','trajectory')
% 
param.gates = [10 10 10 10 40;
               5  15 25 35 20]';
bouy_colour = [1 0 0;
               .75 .75 .75;
               .75 .75 .75;
                0   1   0 ;
                0   0   0 ];
figure(2)
clf
hold on
plot(xo,yo)
scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')
title('Bearing Estimator Output')
hold off

figure (3)
plot(t,Theta_ref-Theta_Beam)
title('Error (deg)')
xlabel('Time')
ylabel('Error')
grid on

% plot(x,y,':xr')
% hold off
