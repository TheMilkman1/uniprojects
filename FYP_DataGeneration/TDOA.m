function [ theta ] = TDOA( Sig1, Sig2, d ,c,TS)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [xc,lags] = xcorr(Sig2,Sig1);
    [~,I] = max(abs(xc));
    lags = lags.*TS;  % account for sample time
    theta = 180-acosd((lags(I)*c)/(d*3));

end

