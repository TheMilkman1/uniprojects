% Initilise
clear all
close all
clc
clf

mex BeamForm3.c  % Let's get mexy
SourceEntry = [10 10 0]';  % Define entry gate

XY=.5*[1,  0.9;   1,2; 1.5,3;  2,2;   2,-2; 1,-2;  1,-1.25;
      -1,-1.25; -1,-2; -2,-2; -2,2; -1.5,3; -1,2; -1, .9]; 
param.gates = [10 10 10 10 40;
               5  15 25 35 20]';
bouy_colour = [1   0    0;
               .75 .75 .75;
               .75 .75 .75;
                0   1   0 ;
                0   0   0 ];
% Approach trajectory
n = 21;                   % Number of points on approach
xo = linspace(-30, 10, n);  % Defining approach
yo = 25+ -6*sin(xo*.13);    % PF Approach
%yo = linspace(30, 10, n);  % UKF Approach
t = linspace(0, 100,n-1);


% % Check trajectory
% plot(xo,yo)
% scatter(SourceEntry(1),SourceEntry(2),'x')
% scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')    
% hold off
%% Parameters
hdist = 0.018;             % Distance between hydrophones in the array (m)

c = 1500;                       % Sound Speed (approx) (m/s)
TS = 1e-6 ;                     % Time Step (sample rate) (sec)

%% Loopy loop

for k = 2:n-1
    % Source Signal Construction 
    %tic
    Cent = [xo(k) yo(k) 0]';  % Centroid of hydrophone array
    psi = atan2d(yo(k)-yo(k-1),xo(k)-xo(k-1));                % WAM-V Orientation;
    %t1 = toc
    %tic
    [S1, S2, S3, S4, tt, H] = SigGen(Cent, psi, hdist,SourceEntry);

    signal = [S1 S2 S3 S4];
    %t2 = toc
    %tic
    % Beamformer
    [Theta_Beam(k),~] = delayBF(signal,TS,c,hdist,4);
    %t3 = toc
    %tic
    % TDOA
    Theta_TDOA(k) = TDOA(S1, S4, hdist ,c,TS);
    %t4 = toc
    %tic
    T_mex(k) = BeamForm3(S1, S2, S3, S4);
    %t5 = toc
    Theta_ref(k) = (atan2d(SourceEntry(2)-Cent(2),SourceEntry(1)-Cent(1))-psi);

end

%% Plotting
figure(2)
plot(t,-Theta_ref,'r',t,Theta_TDOA,t,Theta_Beam,t,T_mex)
legend('Reference Angle','TDOA','Beamformer', 'Mexed Beamformer')
grid on

% k=1;
% figure(1)
% while k < length(t)
%     hold off
%     plot(xo,yo,':r') %plot trajectory   
%     axis([-35 25 -5 45], 'equal')   
%     hold on
%     cent = [xo(k) yo(k) 0]';  % Centroid of hydrophone array
%     psi = atan2d(yo(k+1)-yo(k),xo(k+1)-xo(k));                % WAM-V Orientation;
%     
%     scatter(SourceEntry(1),SourceEntry(2),'x')
%     scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')
% 
%     legend('Source', 'trajectory')
%     xlabel('Length(m)')
%     ylabel('Width(m)')
%     title('Competition Field')
% 
%     
%     plot([xo(k), 60*cosd(psi+Theta_ref(k))+xo(k) ],[yo(k),60*sind(psi+Theta_ref(k))+yo(k)],':')
%     plot([xo(k), 60*cosd(psi-Theta_ref(k))+xo(k) ],[yo(k),60*sind(psi-Theta_ref(k))+yo(k)],':')
%     h = patch(XY(:,1) + cent(1),XY(:,2) + cent(2),'blue'); 
%     
%     rotate(h,[0 0 90],psi-90,cent);
% 
%     k=k+1;
%     pause(eps)
% end


figure (3)
plot(t,Theta_ref+Theta_Beam)
title('Error')
xlabel('Time')
ylabel('Error (deg)')
grid on

