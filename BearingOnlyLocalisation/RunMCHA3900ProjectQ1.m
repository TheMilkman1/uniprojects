%% Run MCHA3900 Q1
clear all
clf
clc

%% Generates Trajectory and tests to see if its all working
time = linspace(5,505,1400);

for r = 1:length(time)-1
    
    E(r) = 0.18*time(r);             % x-position
    N(r) = 21+10*cosd(.59*time(r));   % y-position
    
    E_next = 0.18*time(r+1);
    N_next = 21+10*cosd(.59*time(r+1));
    
    theta(r) = atan2d(E_next-E(r),N_next-N(r));      
end

hold on
% 
% % Creates the boat picture
% XY=.5*[1,  0.9 ;  1, 2; 1.5, 3;  2,2;  2, -2; 1,-2; 1,-1.25;
%        -1,-1.25; -1,-2; -2, -2; -2,2; -1.5,3; -1,2; -1, 0.9];
% 
% scatter(E,N)
% k=1;
% % Test plots trajectory with boat to indicate working direction
% while k < (length(time)-1)   %Build Shape 
%    hold off 
%    h = patch(XY(:,1) + E(k),XY(:,2) + N(k),'blue'); 
%    
%    origin = [E(k) ,N(k),0];
%    rotate(h,[0 90],-theta(k),origin);
%    hold on
%    k=k+15;
%    pause(eps) 
% end


% Generates Map
param.gates = [10 10 10 10 80 80 80 80;
               5  15 25 35  5 15 25 35]';
           
n=0.15;

scatter(E,N)


%%    
% Simulation parameters
T = 1;

param.Q = diag([1e-5, 1e-5, 1e-1,1e-12,1e-12,1e-9]);
param.T = T;

% Initial estimation error covariance
P = diag([15,15,15,1,1,1]);
P_UKF = P;

%% Problem 4
% Initial state with noise
x_UKF = [10*rand+N(1);10*rand+E(1);5*rand+theta(1);0;0;0]; %N E psi u v r

param.var = 1^2;            %variance of noise

% Initilise stuff
x_UKF_hist = zeros(length(x_UKF),length(time));
Pdiag_UKF_hist = zeros(length(x_UKF),length(time));
y_hist = zeros(length(param.gates),length(time));
gn = zeros(length(param.gates),length(time));
ge = zeros(length(param.gates),length(time));

% Estimate states
for i = 1:length(time)-1
    
        u = [0;0;0;0;0;0;]; %Input vector
        
        % Generate the bearings through the measurement model
        [y,~,~,~] = measurementModelQ1([N(i);E(i);theta(i);0;0;0], u,param);
        
        y = y + param.var*randn(length(param.gates),1);
        [x_UKF,P_UKF] = UKF(P_UKF,x_UKF,u,y,@processModelQ1,@measurementModelQ1,param);
        x_UKF_hist(:,i) = x_UKF;
        Pdiag_UKF_hist(:,i) = diag(P_UKF);
        y_hist(:,i) = y;

        gn(:,i) = 80*cosd(y) + x_UKF_hist(1,i); % N(i) for plotting bearings of gates
        ge(:,i) = 80*sind(y) + x_UKF_hist(2,i); % E(i) for plotting bearings of gates
end

% Boat Picture
XY=.5*[1,0.9; 1,2; 1.5,3; 2,2; 2,-2; 1,-2; 1,-1.25;
    -1,-1.25; -1,-2; -2,-2; -2,2; -1.5,3; -1,2; -1,.9]; 

k=1;

% % Video Part 1/3
% delete('bearing2.avi')
% bearing2 = VideoWriter('bearing2.avi')
% open(bearing2)

figure(1)
clf

while k < length(time)
    hold off 
    scatter(E,N,5,'.r') % Reference Traj

    axis([0,95,0,40])
    grid on
    hold on

    scatter(param.gates(:,1),param.gates(:,2),1400,'.g')

    xlabel('East Estimate')
    ylabel('North Estimate')
    title('State Estimate From Noisy Bearing Data (UKF)')

    plot([x_UKF_hist(2,k), ge(1,k)],[x_UKF_hist(1,k), gn(1,k)],':k',...
         [x_UKF_hist(2,k), ge(2,k)],[x_UKF_hist(1,k), gn(2,k)],':k',...
         [x_UKF_hist(2,k), ge(3,k)],[x_UKF_hist(1,k), gn(3,k)],':k',...
         [x_UKF_hist(2,k), ge(4,k)],[x_UKF_hist(1,k), gn(4,k)],':k',...
         [x_UKF_hist(2,k), ge(5,k)],[x_UKF_hist(1,k), gn(5,k)],':k',...
         [x_UKF_hist(2,k), ge(6,k)],[x_UKF_hist(1,k), gn(6,k)],':k',...
         [x_UKF_hist(2,k), ge(7,k)],[x_UKF_hist(1,k), gn(7,k)],':k',...
         [x_UKF_hist(2,k), ge(8,k)],[x_UKF_hist(1,k), gn(8,k)],':k')
    hold on
    
    % plots boat trail
    plot(x_UKF_hist(2,1:k),x_UKF_hist(1,1:k),'b')

    % Builds boat shape 
    h = patch(XY(:,1) + x_UKF_hist(2,k),XY(:,2) + x_UKF_hist(1,k),'blue'); 

    theta2 = x_UKF_hist(3,k);     % Using estimated phi
    origin = [x_UKF_hist(2,k) ,x_UKF_hist(1,k),0];
    rotate(h,[0 90],-theta2,origin);  

%     % Video Part 2/3   
%     frame = getframe(gcf);
%     writeVideo(bearing2, frame);   

    k=k+1;
    %pause(eps) 
end
hold on

% % Video Part 3/3
% close(bearing2)

grid on
xlabel('E estimate')
ylabel('N estimate')
title('NE estimate from noisy bearing data (UKF)')

% figure(2);
% clf
% semilogy(time,Pdiag_UKF_hist)
% title('diag(P) for UKF')
% xlabel('N (samples)')
% grid on