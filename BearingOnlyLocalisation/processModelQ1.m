function [xnext,A,B,Q] = processModelQ1(x,~,param)
%#codegen
N = x(1);
E = x(2);
psi = x(3);
u = x(4);
v = x(5);
r = x(6);
T = param.T;

% This is the Euler discretisation
% xnext =  [N + T*u;
%           E + T*v;
%           psi + T*r; 
%           u;
%           v;
%           r];
      
xnext =  [N+T*u*cosd(psi)+T*v*sind(psi);
          E+T*u*sind(psi)+T*v*cosd(psi);
          psi+T*r; %%%%%%%%%%%%%%%%%%%%%%%%%%
          u;
          v;
          r];
A = 0;
B = 0;

% Covariance of additive process noise
Q = param.Q;