function [y,C,D,R] = measurementModelQ1(x,~,param)

N = x(1);
E = x(2);
%psi = x(3);
Nm = param.gates(:,2);
Em = param.gates(:,1);

y = atan2d(Em-E,Nm-N);
C = 0;
D = 0;
% Covariance of additive measurement noise
R = param.var*eye(size(param.gates,1));