#include "controller.h"

static float angle_state[2];
static float velocity_state[2];

// Cascade Gains
static float 	A1=0.7755,
				B1=3.384,
				C1=6.292,
				D1=-119;

static float 	A2=0.0000,
				B2=0.0000,
				C2=0.0000,
				D2=0.0402;

// Cascade Gains 2 - With intergrator in outer
//static float 	A1=0.8528,
//				B1=2.522,
//				C1=2.949,
//				D1=-74.84;
//
//static float 	A2=1,
//				B2=0.0007906,
//				C2=-0.0004427,
//				D2=0.035;


// State feedback Gains
static float	K1=-27.6700,   
				K2=-6.3382,
				K3=0,
				K4=-0.7306;


float state_feedback_error(float state1, float state2, float state3, float state4)
{
	float e=K1*state1 + K2*state2 + K3*state3 + K4*state4;
	return e;
}

// Initialise cascade controller states at zero
void ctrl_init(void)
{
	angle_state[1]=0;
	velocity_state[1]=0;
	angle_state[0]=0;
	velocity_state[0]=0;
}

float angle_controller(float input) 
{
    //  output = Cx[k] + Du
	float y = C1*angle_state[0] + D1*input;

	//  x[k+1] = Ax[k] + Bu
	angle_state[1]=A1*angle_state[0] + B1*input; //x[k+1] = Ax[k] + Bu

	angle_state[0]=angle_state[1];

	return y;
}

float velocity_controller(float input)
{
    //  output = Cx[k] + Du
	float y1 = C2*velocity_state[0] + D2*input;
	
	//  x[k+1] = Ax[k] + Bu
	velocity_state[1] = A2*velocity_state[0] + B2*input;

	velocity_state[0] = velocity_state[1];

	return y1;
}

