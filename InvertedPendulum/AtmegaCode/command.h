#ifndef COMMAND_H
#define COMMAND_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <avr/io.h>
#include "event_timer.h"
#include "ctrlalloc.h"
#include "motor.h"

#define MAX_WORDS 10	//Memory allocation for the maximum number of words allowed in the string paraser
#define MAX_CHARS 20	//Memory allocation for the maximum number of charater allowed in the string paraser

// Function Prototypes
int  string_parser(char* inp, char* array_of_words[]);
void cmd_help(int n, char* args[]);
void cmd_thetaeq(int n, char* args[]);
void cmd_veq(int n, char* args[]);
void cmd_vref(int n, char* args[]);
void cmd_chip(int n, char* args[]);
void cmd_ctrl(int n, char* args[]);
void cmd_thetais(int n, char* args[]);
void cmd_velis(int n, char* args[]);
void cmd_velrefis(int n, char* args[]);
void cmd_vctrl(int n, char* args[]);
void cmd_enc(int n, char* args[]);
void cmd_mot(int n, char* args[]);
void cmd_foris(int n, char* args[]);
void cmd_feq(int n, char* args[]);
void cmd_calloc(int n, char* args[]);
void cmd_hil(int n, char* args[]);
void cmd_c(int n, char* args[]);


char* word_array[MAX_WORDS];	// CMD pointer to where each word will be stored.

typedef struct
{
        const char * cmd;
        void (*func)(int argc, char *argv[]);
        const char * help;
} CMD_T;

static CMD_T cmd_table[] =
{
	   {"s",         cmd_stop,       "Shutdown motors and reset commands"},
	   {"balc",      cmd_balc,       "Balance for 'n' samples using a cascade controller(balc 10000)"},
	   {"bals",      cmd_balss,      "Balance for 'n' samples using state feedback (bal2 10000)"},	   
	   {"v",         cmd_vref,       "Sets a value for desired relitive velocity (v .5)ms"},
	   {"cal",       cmd_calpot,     "Calibrate pot by inc/decrementing offset (cal -1)"},
	   {"cal?",      cmd_calpotis,   "Displays the current POT offset"},
	   {"slope",     cmd_slopeoff,   "Sets and displays slope offset in degrees"},
	   {"mgain",     cmd_mgain,      "Sets a new motor gain (mgain 70)"},
	   {"mgain?",    cmd_mgaineq,    "Displays the current motor gain"},
	   {"chip",      cmd_chip,       "Displays ATmega32 pin configuration"},
	   {"help",      cmd_help,       "Displays the command table but you already know that, don't you?"},
	   //{"pot",       cmd_pot,        "Displays/streams pot reading for 'n' samples (pot 10000)"},
	   //{"enc?",      cmd_enc,        "reads encoder count"},
	   //{"mot",       cmd_mot,        "Send motor duty"},

	   	   
		// ------- HIL command set -------- //
	   //{"hil",       cmd_hil,        "receives parameters for HIL"},
	   //{"c",         cmd_c,          "return control force for HIL"},
	   //{"ctrlalloc?",cmd_calloc,     "Displays force going to control alloc"},
	   //{"force",     cmd_feq,        "sets force going to control alloc"},
	   //{"theta?",    cmd_thetais,    "Displays pendulum angle"},
	   //{"vel?",      cmd_velis,      "Displays cart velocity"},
	   //{"velref?",   cmd_velrefis,   "Displays cart velocity"},
	   //{"theta",     cmd_thetaeq,    "Sets a value for pendulum angle"},
	   //{"vel",       cmd_veq,        "Sets a value for car velocity"},
	   //{"sin",       cmd_sin_table,  "sin table; samples amp freq"},
	   //{"force?",    cmd_foris,      "Displays force going to control alloc"},
	   //{"ctrl?",     cmd_ctrl,       "Perform/display the control calculations for a time step"},
	   //{"vctrl?",    cmd_vctrl,      "Displays 'vctrl'"},

};
enum {NUM_CMDS = sizeof(cmd_table)/sizeof(CMD_T)};

#endif
