#ifndef MOTOR_H
#define MOTOR_H

void pot_init(void);
void motor_init(void);      // Configure ADC and Timer1
void motorrun(float v);
float motor_pos(void);
float motor_vel(void);
void cmd_mgain(int n, char* args[]);
void cmd_mgaineq(int n, char* args[]);

int xe;

#endif
