#ifndef ENCODER_H
#include <avr/io.h>
#include <stdio.h>
#include <ctype.h> 
#include <stdlib.h>
#include <avr/interrupt.h>
#include <stdbool.h>


void enc_init(void);
void enc_reset(void);
int  enc_read(void);

#endif
