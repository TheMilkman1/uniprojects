#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "command.h"
#include "controller.h"
#include "motor.h"
#include "encoder.h"
#include "event_timer.h"
#include "ctrlalloc.h"

static float theta,c1;
static float vel, force;
static float velref;
static volatile int event_count;


void cmd_hil(int n, char* args[])
{
	n=-1.7775;
	float theta2 = atof(args[1]);
    float omega2 = atof(args[2]);
    float x2 =     atof(args[3]);
	float v2 =     atof(args[4]);
	
	float c5 = velref*n - state_feedback_error(theta2, omega2, x2, v2);
		
	c1= cont_alloc(c5, v2);
}

void cmd_c(int n, char* args[])
{
	printf_P(PSTR("%g\n"), c1);
}


// Separates an array of words separtated by a space or '=' for command table

int string_parser(char* inp, char* array_of_words[])
{
	unsigned int word_count=0;			//Initialise count of number of words at 0. unsigned to suppress warning
  	char* string = strtok (inp," =");	//Temporary pointer to a string where the string is split into tokens determined by the delimiters.             
  	while(string != NULL && word_count<MAX_WORDS)//While the NULL term isn't found and the word array doesn't overflow, each word is stored in the next loaction in word_array.
	{
		array_of_words[word_count]=string;
		word_count++;
		string = strtok(NULL," =");
	}
	//If the word array would have overflowed, an error message is printed explaining this
	if(word_count>=MAX_WORDS)
		printf("Too many words in input string, make MAX_WORDS larger to accommodate\n");

	return (word_count);
}

// Sets motor voltage
void cmd_mot(int n, char* args[])
{
	motorrun(atof(args[1]));
}

// Prints the command help table
void cmd_help(int n, char* args[])
{
	printf_P(PSTR("   \n Command List\n-----------------------------------------------------------------------\n"));
	for (int i = 0; i < NUM_CMDS; i++)
	{
		printf_P(PSTR("| %-10s | %s\n"), cmd_table[i].cmd, cmd_table[i].help);
	}
	printf_P(PSTR("-----------------------------------------------------------------------\n\n"));
}

void cmd_enc(int n, char* args[])
{
	printf_P(PSTR("encoder count = %d\n"), enc_read());
}

void cmd_calloc(int n, char* args[])
{
	printf_P(PSTR("control alloc=%f\n"), cont_alloc(force, vel));
}



// Displays the configuration of the ATmega32
void cmd_chip(int n, char* args[])
{
	printf_P(PSTR("ATmega32 pin configuration:\n\n"));
	printf_P(PSTR("                   (XCK/T0)PB0 <-> 1 +---\\_/---+40 <-- PA0(ADC0) : Potentiometer  \n"));
	printf_P(PSTR("                       (T1)PB1 <-> 2 |         |39 <-- PA1(ADC1)  \n"));
	printf_P(PSTR("                (INT2/AIN0)PB2 <-> 3 |    A    |38 <-- PA2(ADC2)  \n"));
	printf_P(PSTR("                 (OC0/AIN1)PB3 <-> 4 |    T    |37 <-- PA3(ADC3)  \n"));
	printf_P(PSTR("                      (!SS)PB4 <-> 5 |    M    |36 <-- PA4(ADC4)  \n"));
	printf_P(PSTR("                     (MOSI)PB5 <-> 6 |    E    |35 <-- PA5(ADC5)  \n"));
	printf_P(PSTR("                     (MISO)PB6 <-> 7 |    L    |34 <-- PA6(ADC6)  \n"));
	printf_P(PSTR("                      (SCK)PB7 <-> 8 |         |33 <-- PA7(ADC7)  \n"));
	printf_P(PSTR("                        !RESET --> 9 |    A    |32 <-> AREF       : Linked to GND via capacitor     \n"));
	printf_P(PSTR("                 5V:       VCC --> 10|    T    |31 <-> GND        \n"));
	printf_P(PSTR("                           GND <-> 11|    m    |30 <-- AVCC       : Linked to VCC (5V)          \n"));
	printf_P(PSTR("    14.7456MHz XTAL:     XTAL2 <-- 12|    e    |29 <-- PC7(TOSC2) \n"));
	printf_P(PSTR("    14.7456MHz XTAL:     XTAL1 --> 13|    g    |28 <-- PC6(TOSC1  \n"));
	printf_P(PSTR("  Serial/Bluetooth :  (RXD)PD0 --> 14|    a    |27 <-- PC5(TDI)   \n"));
	printf_P(PSTR("  Serial/Bluetooth :  (TXD)PD1 <-- 15|    3    |26 --> PC4(TDO)   : INB on Motor 1\n"));
	printf_P(PSTR("           Encoder : (INT0)PD2 --> 16|    2    |25 --> PC3(TMS)   : INA on Motor 1\n"));
	printf_P(PSTR("           Encoder : (INT1)PD3 --> 17|         |24 <-- PC2(TCK)   : Bootloader button\n"));
	printf_P(PSTR("        PWM for M1 : (OC1B)PD4 <-- 18|         |23 --> PC1(SDA)   : INB on Motor 2\n"));
	printf_P(PSTR("        PWM for M2 : (OC1A)PD5 <-- 19|         |22 --> PC0(SCL)   : INA on Motor 2\n"));
	printf_P(PSTR("                     (ICP1)PD6 <-- 20+---------+21 <-- PD7(OC2)   \n"));
}

// -------------- HIL Functions ---------------//

// Prints/Sends Theta to terminal/matlab
void cmd_thetais(int n, char* args[])
{
	printf_P(PSTR("theta=%f\n"), theta);
}

// Prints/Sends velocity of the cart to terminal/matlab
void cmd_velis(int n, char* args[])
{
	printf_P(PSTR("vel=%f\n"), vel);
}

// Prints/Sends the output of the velocity controller to terminal/matlab
void cmd_vctrl(int n, char* args[])
{
	printf_P(PSTR("vctrl=%f\n"), velocity_controller(velref-vel));
}

// Prints/Sends Theta to terminal/matlab
void cmd_foris(int n, char* args[])
{
	printf_P(PSTR("force=%f\n"), force);
}

// Prints/Sends reference velocity to terminal/matlab
void cmd_velrefis(int n, char* args[])
{
	printf_P(PSTR("velref=%f\n"), velref);
}

// Sets pendulum angle in radians
void cmd_thetaeq(int n, char* args[])
{
	theta = atof(args[1]);
}

// Sets the cart velocity
void cmd_veq(int n, char* args[])
{
	vel = atof(args[1]);
}

// Sets the required force
void cmd_feq(int n, char* args[])
{
	force = atof(args[1]);
}

// Proform controller calculations
void cmd_ctrl(int n, char* args[])
{
	float a=velocity_controller(velref-vel);
	printf_P(PSTR("ctrl=%f\n"),angle_controller(a-theta));
}

// Be careful, funtion currently implimented in event_timer.c
// kept incase of an impromptu HIL test later on.

//void cmd_vref(int n, char* args[])
//{
//	velref = atof(args[1]);
//}
