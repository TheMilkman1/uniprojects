#include "ctrlalloc.h"

// Initialise control allocation constants
void cont_alloc_init(void)
{
	Kcont   = 0.0101;  // Motor Constant
	rcont   = 0.039;   // Wheel radius [mm]
	etacont = 0.6932;  // Gearbox Efficiency
	Ncont   = 30;      // Gear ratio 
	taumcont= 0.0056;  // Internal Motor Resistance
	Rcont   = 3;       // Armature resistance (Ohm)

	Acont = (Rcont*rcont)/(Kcont*etacont*Ncont);
	Bcont = (Kcont*Ncont)/rcont;
	Ccont = (Rcont*taumcont)/Kcont;
}

// Takes the force required and current velocity
// Returns voltage to be send to motors 
float cont_alloc(float For, float Vel)
{
	if (For>0)
		return Acont*For + Bcont*Vel + Ccont;
	if (For<0)
		return Acont*For + Bcont*Vel - Ccont;
	else
		return Bcont*Vel;
}
