#ifndef CTRLALLOC_H
#define CTRLALLOC_H

void  cont_alloc_init(void);            // Initialise Gains
float cont_alloc(float For, float Vel); // Allocate Control

float   Kcont,		// Motor Constant (-)
		rcont,		// Wheel radius [m]
		etacont,	// Gearbox Efficiency (-)
		Ncont,		// Gear ratio (N:1)
		taumcont,	// Internal Motor Resistance (N.m)
		Rcont,		// Armature resistance (Ohm)
		Acont,		// Gain A
		Bcont,		// Gain B
		Ccont;		// Gain C
#endif
