#ifndef SYSID_H
#define SYSID_H

// Function Prototypes
void event_init(void);
int  event_pending(void);
void event_reset(void);
void event_start(void);
void event_stop(void);
void event_action(void);
void event_set(void (*func)(void));
void reset_time(void);

void cmd_sin_table(int n, char* args[]);
void event_sin_table(void);

void cmd_pot(int n, char* args[]);
void event_pot(void);

void cmd_balc(int n, char* args[]);
void event_balc(void);

void cmd_balss(int n, char* args[]);
void event_balss(void);

void cmd_stop(int n, char* args[]);

void cmd_calpot(int n, char* args[]);
void cmd_calpotis(int n, char* args[]);

void cmd_slopeoff(int n, char* args[]);

// Saves previous pend. angle for ang. velocity calcs
float last_theta;


#endif
