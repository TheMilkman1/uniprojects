#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <math.h>
#include <inttypes.h>
#include "motor.h"
#include "encoder.h"

#define XENC 0.136//1276272 //mm per encoder count.(2*pi*r)/(enc_ratio*gear_ratio)

int Mgain=70; // Motor Gain constant, helps on varying frictons (70 for benchtop)

// Configure ADC for reading pot
void pot_init(void)
{
    ADMUX = (1<<REFS0)|(1<<REFS1);         // ADC referenced with 2.52 internal voltage
    ADCSRA= (1<<ADEN)|(1<<ADPS1)	       // Set prescaler to 1/128 and enable ADC  
		   |(1<<ADPS2)|(1<<ADPS0);         // Works fine with 1/64, less accurate?
}
ISR(TIMER1_OVF_vect) {}

// Configure motors
void motor_init(void)
{
	// COM1(A|B)(1|0) ports are all set with WGM10 to produce 8bit phase correct pwm on OC1A and OC1B (p109)
	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM10)|(1<<WGM11);

	// Init. duty cycle
    OCR1A = 0x0;
    OCR1B = 0x0;

	// Prescal PWM to clk/8. slowww it dowwnnnnn
	TCCR1B |= (1<<CS11); //CS10 for approx 7kHz, makes motor driver run really hot

	// Set OC1(AB) as output. DDRD is port D sets pin4/5 as 1 (output)
	DDRD |= (1<<4)|(1<<5);
}

// Setting the motor gain constant
void cmd_mgain(int n, char* args[])
{
	Mgain= atof(args[1]);
	printf_P(PSTR("New motor gain is %d\n"), Mgain);
}

// Displaying the current moto gain
void cmd_mgaineq(int n, char* args[])
{
	printf_P(PSTR("Motor gain is %d\n"), Mgain);
}

// Motor Run function runs motors backward if negative voltage and forward if positive.
void motorrun(float vlt)
{   
	vlt *=Mgain;
	
	// Limit the PWM signal sent to the motors
	if(vlt>1000)
		vlt=999;
	if(vlt<-1000)
		vlt=-999;
	

    if (vlt>0)     // Tests if forward, backwards or stop
    {
		PORTC = (1<<0)|(0<<1)|(0<<3)|(1<<4); // Sets pins for forward
    	OCR1A = vlt;
    	OCR1B = vlt;
    }
     
    else if (vlt<0)
    {
    	PORTC = (0<<0)|(1<<1)|(1<<3)|(0<<4); // Sets pins for reverse
    	OCR1A = -vlt; // Registers don't like negitive values
    	OCR1B = -vlt; // Registers don't like negitive values
    }
     
    else if (vlt==0) // Stop motors
    {
    	//PORTB = (0<<4)|(0<<5)|(0<<6)|(0<<7); // Sets pins for breaking
		OCR1A = 0; 
    	OCR1B = 0;
    }
 
}

// Returns the position of the motor
float motor_pos(void)
{
	float x=enc_read()*XENC/1000; // enc* enc_convert /1000(to m)/0.01 (time)
	return x;
}

// Returns motor velocity based on being called every 10ms
float motor_vel(void)
{
	ATOMIC_BLOCK(ATOMIC_FORCEON) 
	{	
		xe=enc_read();
		enc_reset();
	}
	return xe*XENC*0.1; // enc* enc_convert /1000(to m)/0.01 (time);
}
