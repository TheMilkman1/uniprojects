###############################################################################
# Makefile for the project rubegoldberg
###############################################################################

## General Flags
PROJECT = rubegoldberg
MCU = atmega32
TARGET = rubegoldberg.elf
CC = avr-gcc

CPP = avr-g++

## Options common to compile, link and assembly rules
COMMON = -mmcu=$(MCU)

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
CFLAGS += -Wall -gdwarf-2 -std=gnu99                                 -DF_CPU=14745600UL -Os -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums
CFLAGS += -MD -MP -MT $(*F).o -MF dep/$(@F).d 

## Assembly specific flags
ASMFLAGS = $(COMMON)
ASMFLAGS += $(CFLAGS)
ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Linker flags
LDFLAGS = $(COMMON)
LDFLAGS += -Wl,-u,vfprintf -Wl,-Map=rubegoldberg.map


## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom -R .fuse -R .lock -R .signature

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings


## Libraries
LIBS = -lprintf_flt -lm 

## Objects that must be built in order to link
OBJECTS = main.o uart.o line_buffer.o circ_buffer.o encoder.o command.o controller.o motor.o event_timer.o ctrlalloc.o 

## Objects explicitly added by the user
LINKONLYOBJECTS = 

## Build
all: $(TARGET) rubegoldberg.hex rubegoldberg.eep rubegoldberg.lss## Compile
main.o: ../main.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

uart.o: ../uart.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

line_buffer.o: ../line_buffer.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

circ_buffer.o: ../circ_buffer.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

encoder.o: ../encoder.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

command.o: ../command.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

controller.o: ../controller.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

motor.o: ../motor.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

event_timer.o: ../event_timer.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

ctrlalloc.o: ../ctrlalloc.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

##Link
$(TARGET): $(OBJECTS)
	 $(CC) $(LDFLAGS) $(OBJECTS) $(LINKONLYOBJECTS) $(LIBDIRS) $(LIBS) -o $(TARGET)

%.hex: $(TARGET)
	avr-objcopy -O ihex $(HEX_FLASH_FLAGS)  $< $@

%.eep: $(TARGET)
	-avr-objcopy $(HEX_EEPROM_FLAGS) -O ihex $< $@ || exit 0

%.lss: $(TARGET)
	avr-objdump -h -S $< > $@

## Clean target
.PHONY: clean
clean:
	-rm -rf $(OBJECTS) rubegoldberg.elf dep/* rubegoldberg.hex rubegoldberg.eep rubegoldberg.lss rubegoldberg.map


## Other dependencies
-include $(shell mkdir dep 2>NUL) $(wildcard dep/*)

