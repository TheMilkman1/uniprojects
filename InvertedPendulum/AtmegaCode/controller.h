#ifndef CONTROLLER_H
#define CONTROLLER_H

void ctrl_init(void);
float angle_controller(float input);
float velocity_controller(float input);
float state_feedback_error(float theta, float omega, float x, float v);

#endif
