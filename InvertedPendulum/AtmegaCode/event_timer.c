#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <math.h>
#include <inttypes.h>
#include "event_timer.h"
#include "motor.h"
#include "controller.h"
#include "ctrlalloc.h"

#define TSTEP 0.01
#define DEG2RAD 0.0174533

/*********** Pot tuning parameters *********/
#define ADC2RAD 500       // ADC to radians conversion rate
static int potoff=454;    // Pot Offset
static float slopeoff=0;  // Slope offset
/*******************************************/

static volatile unsigned int event_count; // Event counter
static float time, amplitude, frequency;  // Constants for sin sysid test
static unsigned int nsamples;             // Number of samples for sysid tests
static void (*event_func)(void);          // Pointer to event handler

static float vref=0;        // Reference Velocity
static float N=-0.7210;     // Refernce gain

// ----------- Balance Using State Feedback ----------- //
void cmd_balss(int n, char* args[])
{
	if (n!=2)
	{
		printf_P(PSTR("Bad syntax, you need help..."));
		return;     // Bail out if the syntax is poor
	}
	nsamples = atof(args[1]);
	vref=0;                    // Reset reference velocity
	last_theta=0;              // Reset the temp theta varible
	printf_P(PSTR("\nGoodluck!!!\n"));
	event_set(event_balss);    // Set the event to balance with state space
	event_start();             // Sets/turns on output compare match interrupt
}

// ------ Controller 2 Event - State Feedback -------- //

void event_balss(void)
{						
	if (nsamples)
	{		
		ADCSRA |= (1<<ADSC);                           // Start conversion  (may timeout event timer)

		float x_ss=motor_pos();
		float v_ss=motor_vel();

		while(ADCSRA &(1<<ADSC));                      // ADSC is cleared once conversion is complete
		float theta_ss =((float) ADC-potoff)/ADC2RAD;  // Converts ADC reading to radians
		float omega_ss=(theta_ss-last_theta)/TSTEP;    // Calculates angular velocity (omega)
		last_theta=theta_ss;		

		float f_ss = vref*N - state_feedback_error(theta_ss, omega_ss, x_ss, v_ss); // Performs vel tracking state feedback 
		
		float cont_force_ss= cont_alloc(f_ss, v_ss);   // Allocates control
	
		if(theta_ss<0.4 && theta_ss>-0.4)              // Safety cuttoff just beyond crit.angle
			motorrun(cont_force_ss);                   // Send force to the motors

		else
		{
			event_stop();     // Resets event timer if crit. angle exceeded
			printf_P(PSTR("\nYou're just too much!\nPlay nicer next time!\n"));
		}
		nsamples--;
	}
	else
	{
		event_stop();
		printf_P(PSTR("\nTime's up!\n"));
	}
}

// -------- Controller 1 Event - Cascade Control ------- //

void cmd_balc(int n, char* args[])
{
	if (n!=2)
	{
		printf_P(PSTR("Bad syntax, you need help..."));
		return;
	}
	nsamples = atof(args[1]);
	printf_P(PSTR("\nGoodluck!!!\n"));
	time=nsamples*TSTEP;
	event_set(event_balc);
	event_start(); // Sets/turns on output compare match interrupt
}

void event_balc(void)
{						
	if (nsamples)
	{		
		ADCSRA |= (1<<ADSC);            // Start conversion  (may timeout event timer)
		
		float v_c = motor_vel();
		float v_cont=velocity_controller(vref-v_c);
		
		while(ADCSRA &(1<<ADSC));       // ADSC is cleared once conversion is complete
		
		float theta_c =((float) ADC-potoff)/ADC2RAD-(slopeoff*DEG2RAD);        // Converts ADC reading to radians, accounts for slope
		float cont_force_c= cont_alloc(angle_controller(v_cont-theta_c), v_c); // Allocates control

		if(theta_c<0.4 && theta_c>-0.4) // Safety cuttoff just beyond crit.angle
			motorrun(cont_force_c);     // Send force to the motors

		else
		{
			event_stop();
			printf_P(PSTR("\nYou're just too much!\nPlay nicer next time!\n"));
		}
		nsamples--;
	}
	else
	{
		event_stop();
		printf_P(PSTR("\nTime's up!\n"));
	}
}

// Calibrates potentiomter through terminal and displays new offset
void cmd_calpot(int n, char* args[])
{
	int cal = atoi(args[1]);

	if(cal==1)
		potoff++;

	else if(cal==-1)
		potoff--;

	else
		printf_P(PSTR("Bad syntax, you need help...\n"));

	printf_P(PSTR("Potentionmeter offset = %d\n"),potoff);
}

// --- Various commands related to balancing pendulums and stuff ---- //

// Displays pot offset in terminal
void cmd_calpotis(int n, char* args[])
{
	printf_P(PSTR("Current POT offset: %d\n"),potoff);
}

// Set and displays reference velocity
void cmd_vref(int n, char* args[])
{
	vref = atof(args[1]);
	printf_P(PSTR("Current requested velocity: %.1fm/s\n"),vref);
}

// Set and display slope offset
void cmd_slopeoff(int n, char* args[])
{
	slopeoff = atof(args[1]);
	printf_P(PSTR("Slope offset = %f\n"),slopeoff);
}

void cmd_stop(int n, char* args[])
{
	event_stop();
	
	printf_P(PSTR("What!? Why?\nWe were just getting started!\n"));
}

// ----------- Pot SYSid test --------------------------------------------- //
void cmd_pot(int n, char* args[])
{
	if (n!=2)
	{
		printf_P(PSTR("Bad syntax, you need help..."));
		return;
	}
	nsamples = atof(args[1]);
	printf_P(PSTR("\nTime (sec),Voltage (V)\n"));
	time=0;
	event_set(event_pot);
	event_start(); // Sets/turns on output compare match interrupt
}

void event_pot(void)
{						
	if (nsamples)
	{
		ADCSRA |= (1<<ADSC); // Start conversion  ****going to be too slow
		while(ADCSRA &(1<<ADSC)); // ADSC is cleared once conversion is complete
		
		printf_P(PSTR("%.2f,%d, %.3f\n"),time,ADC-potoff,((float) ADC-potoff)/ADC2RAD);

		time += TSTEP;
		nsamples--;
	}
	else
		event_stop();
}

// ----------- Sin Test - SYSid--------------------------------------------- //
void event_sin_table(void)
{
	if (nsamples)
	{
		float y = amplitude*sin(2*M_PI*frequency*time);
		printf_P(PSTR("%.2f,%g, %.2f\n"),time,y,motor_vel()); // motor vel from mm/s to m/s
		motorrun((int)y);
		time += TSTEP;
		nsamples--;
	}
	else
		event_stop();
}

void cmd_sin_table(int n, char* args[])
{
	if (n!=4)
	{
		printf_P(PSTR("Bad syntax, you need help..."));
		return;
	}
	nsamples = atoi(args[1]);
	amplitude = atof(args[2]);
	frequency = atof(args[3]);
	time = 0;
	printf_P(PSTR("Time (sec),Value\n"));
	event_set(event_sin_table);
	event_start();
}
// -------------------------------------------------------


// -------- Various Event Timer Functions ------------------ //

void event_init(void)             // Init. timer0 to interrupt at 100Hz
{
	TCCR0 |=(1<<WGM01)            // Sets CTC mode on timer1
		  | (1<<CS00)|(1<<CS02)   // Sets prescale to 1024
		  | (1<<COM01);           // Clears OC0 on upcount compare

	//DDRB|=(1<<3);               // Confirm the 100Hz
	
	OCR0 =143;                    // Counts to 143 before interrupting 
}

void event_set(void (*func)(void))
{
	event_func=func;
}

void event_action(void)
{
	event_reset();		  // Reset event counter
	event_func();
}

void event_start(void)
{
	TIMSK |=(1<<OCIE0);   // Turns on timer/counter0 output compare match interrupt
}

int event_pending(void)
{
	return event_count;
}

void event_reset(void)
{
	event_count=0;
}

ISR(TIMER0_COMP_vect)
{
	event_count++;        // Counts how many times timer0 is triggered
}

void reset_time(void)
{
	time=0;
}
void event_stop(void)
{
	TIMSK &=~(1<<OCIE0); // Turns off timer/counter0 output compare match interrupt
	motorrun(0);         // Stops motors
	ctrl_init();         // Resets contollers, might be unnecessary
	event_reset();
}
