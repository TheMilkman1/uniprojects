#include "encoder.h"
#include "circ_buffer.h"
#include <avr/interrupt.h>

static int enc_count;

ISR(INT1_vect)		// USART TX interrupt
{	
	if(((PIND&(1<<PIND3))>>PIND3)==((PIND&(1<<PIND2))>>PIND2))
		enc_count++;		
	else 
		enc_count--;
}

ISR(INT0_vect)		// USART TX interrupt
{	
	if(((PIND&(1<<PIND3))>>PIND3)==((PIND&(1<<PIND2))>>PIND2))
		enc_count--;		
	else 
		enc_count++;
}

void enc_init(void)
{
	GICR  |= (1<<INT1)|(1<<INT0);      // Initilise interrupts
	MCUCR |= (1<<ISC00)|(1<<ISC10);    // Sets encoders for full res. ie any logic change
	enc_count = 0;
}

// Returns raw encoder count
int enc_read(void)
{
	return(enc_count);
}
// Resets encoder count
void enc_reset(void)
{
	enc_count = 0;
}

