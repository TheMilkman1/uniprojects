#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <inttypes.h>

#include "uart.h"
#include "line_buffer.h"
#include "circ_buffer.h"
#include "encoder.h"
#include "command.h"
#include "controller.h"
#include "motor.h"
#include "event_timer.h"
#include "ctrlalloc.h"

static LB_T lb;

static volatile char ch;
static volatile bool ch_waiting;


int main(void)
{
	sei();  	       // Enable Interrupts
	lb_init(&lb);      // Initialise Linebuffer
	uart_init();       // Initialise UART
	enc_init();        // Initialise Encoder
	ctrl_init();       // Initialise Contoller
	motor_init();      // Initialise Motors
	event_init();      // Initialise event timer(0) @ 100Hz
	pot_init();        // Initialise Potentiometer
	cont_alloc_init(); // Initialise Control Allocation
	
	// Start up stuff
	_delay_ms(1000);
	printf_P(PSTR("The snozzberries taste like... Snozzberries!\n"));

	for (;/*ever*/;)
	{
		if (event_pending())
		{	
			event_action();

			if (event_pending())
			{
				printf_P(PSTR("\nEvent taking its sweeet time!\n\n"));
				event_stop();
				event_reset();	
			}
		}

		else // Okay fine, I'll listen...
		{
			while(uart_avail())
			{
				char c = uart_getc();
				if (lb_append(&lb, c) == LB_BUFFER_FULL)  //add to line buffer, check if full
				{
					lb_init(&lb); // Clear line
					printf_P(PSTR("\nMax line length exceeded!\n"));
				}
			}
	
			// Process command if line buffer is terminated by a line feed or carriage return
			if (lb_line_ready(&lb))
			{
			   char n;
			   n = string_parser(lb_gets(&lb), word_array);
			   int i, err_flag =1;

			   for(i=0; cmd_table[i].cmd != NULL; ++i)
			   {
			    	if( strcmp(word_array[0], cmd_table[i].cmd) == 0)
			     	{
		               err_flag=0;
				       cmd_table[i].func(n,word_array);
			       	}
		      }
			  if(err_flag != 0) printf_P(PSTR("Invalid Command Entered\n\n"));	
      
		     lb_init(&lb); // Reset line buffer
			 }
		}
	}
	return 0;
}


