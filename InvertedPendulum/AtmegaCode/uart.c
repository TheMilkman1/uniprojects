#include "uart.h"
#include <avr/interrupt.h>
#include "circ_buffer.h"

// Define and initialise stream used for printing to uart
FILE uart_str = FDEV_SETUP_STREAM(uart_printchar, NULL, _FDEV_SETUP_RW);

static CB_T TX;
static CB_T RX;


ISR(USART_RXC_vect)		// USART RX interrupt
{				
	cb_queue(&RX,UDR);
}


ISR(USART_UDRE_vect)		// USART RX interrupt
{				
	UDR = cb_dequeue(&TX);
	if(cb_empty(&TX))      //  if the buffer is empty then turn off then interrupt 
	{
		UCSRB &= ~(1<<UDRIE);
	}
}

void uart_putc(char c)
{
	while(cb_full(&TX));	// If TX buffer is full, chill here
	
	cb_queue(&TX,c);        // Queue a character into the buffer when ready
	
	UCSRB |= (1<<UDRIE);	// Turn on the UDRE ISR
}

int uart_printchar(char c, FILE * stream)
{
	uart_putc(c);
	return 0;
}

void uart_puts(const char *s)
{
	while (*s) uart_putc(*s++); 	// Loop until end of string
}

void uart_init(void)
{
	// Set baud rate
	UBRRH = (uint8_t)(UART_CALC_BAUDRATE(UART_BAUD_RATE)>>8);
	UBRRL = (uint8_t)UART_CALC_BAUDRATE(UART_BAUD_RATE);

	#ifdef UART_DOUBLESPEED
	UCSRA = (1 << U2X);
	#endif

	cb_init(&RX);
	cb_init(&TX);	

	// Enable receiver and transmitter; enable RX interrupt
	UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);

	// Asynchronous 8N1
	UCSRC = (1 << URSEL) | (3 << UCSZ0);
	
	// Redirect stdout, so that printf() knows where to go
	stdout = &uart_str;
}

int uart_avail(void)
{
	return !cb_empty(&RX);
}

char uart_getc(void)
{
	return cb_dequeue(&RX);
}
