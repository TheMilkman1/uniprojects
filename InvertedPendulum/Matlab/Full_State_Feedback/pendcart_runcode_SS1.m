% Initialise
clc
clear all
%% Cart/pendulum Parameters
M = 1.42767;            % Mass of cart (kg)
m = 0.47282;          	% Mass of pendulum (kg)
l = 0.48887;            % Effective Length (m)
b = 0.01;               % Damping of cart (N.s/m)
c = 0.002;              % Damping of pendulum (N.m.s/rad)
wheel_r = (40)/1000;    % Wheel radius [mm]

%% Motor Parameters
% Data Sheet for 30:1 12V Pololu DC Motor
R=3;                       % Armature resistance (Ohm)
Irun = 300e-3;             % Free Run Current (A)
Tstall = 0.776770701666988;% Stall Motor Torque (N.m)
Um = 12;                   % Supply Voltage
Nm = 30;                    % Gear ratio (N:1)
RPM = 350;                 % Free Run Speed (RPM)
Lm = 0.001;                % Coil Inductance (H)

% Parameters
Istall= Um/R;                  % Stall Current (A)
max_omega = (RPM*2*pi)/60;            % Free Run Angular Velocity (Rad/s) (Convert from rpm: wo=wi*2*pi/60)
Km = (Um-R*Irun)/(Nm*max_omega);        % Motor Constant (-)
taum = Km*Irun;                    % Internal Motor Resistance (N.m)
eta = Tstall/((Km*Istall-taum)*Nm); % Gearbox Efficiency (-)

% Other parameters
g = 9.81;           % Acceleration due to gravity (m/s/s)
alpha = 1*pi/180;   % Ground slope (rad = deg*pi/180)
theta0 = 3*pi/180;  % Initial angle (rad = deg*pi/180)

%% Full State Feedback Controller
A= [      0                1             0   0   ;
       g*(m+M)/(M*l) -c*(m+M)/(m*M*l*l)  0  b/(m*l)  ;
          0               0              0   1   ;
    (-m*g)/M       -c/(M*l)           0 -b/M  ]

B=[0; -1/(M*l);0; 1/M]


C=eye(4)
D=[0 0 0 0]';
Ts= 0.01;
Q=[.1 0 0 0;
   0 .1 0 0;
   0 0 .001 0;
   0 0 0 100];
Rd=1
[Kd,S,e] = lqrd(A,B,Q,Rd,Ts)
N=-9.5057;
%% Run simulation
tsim = 30;
sim('pendcartmodel_SS1');

% Plot results
figure(1);clf
subplot(2,2,1)
plot(t,theta*180/pi)
grid on
xlim([0 tsim])
title('Pendulum Angle')
xlabel('Time (sec)')
ylabel('\theta (\circ)')


subplot(2,2,2)
plot(t,omega*180/pi)
grid on
xlim([0 tsim])
title('Angular Velocity of Pendulum')
xlabel('Time (sec)')
ylabel('\omega (\circ/s)')

subplot(2,2,3)
plot(t,x)
grid on
xlim([0 tsim])
title('Position Of Cart')
xlabel('Time (sec)')
ylabel('x (m)')

subplot(2,2,4)
plot(t,v,t,ref,'r')
grid on
xlim([0 tsim])
title('Velocity Of Cart')
xlabel('Time (sec)')
ylabel('v (m/s)')
legend('Actual','Demanded','Location','Best')

figure(2);clf
plot(t,f)
grid on
title('Control force')
xlabel('Time (sec)')
ylabel('Force (N)')

figure(3);clf
plot(t,m_v)
grid on
title('Motor Voltage')
xlabel('Time (sec)')
ylabel('Voltage (V)')

figure(4);clf
plot(t,m_i)
grid on
title('Motor Current')
xlabel('Time (sec)')
ylabel('Current (A)')

%% LTI Controller
% % Location of plant poles and zeros
% p = sqrt((M+m)*g/M/l);
% q = sqrt(g/l);
% 
% % Inner plant G1
% G1 = zpk([],[-p,p],-1/M/l);
% %  G1 = tf(-1/M/l,[1,0,-p^2]);
% 
% % Inner controller C1
% p1 = 25.4277;
% K1 = -119.0071;
% C1 = zpk(-p,-p1,K1)
% 
% % Outer plant G2
% GFV = zpk([-q,q],[-p,0,p],1/M);
% G2 = minreal(feedback(C1,G1)*GFV);
% 
% % Outer controller C2
% K2 = 0.0402;
% C2 = zpk([],[],K2)
% 
% 
