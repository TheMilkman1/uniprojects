% This function takes as an input the range and number of surface and bottom
% interactions and returns a time domain pressure signal.
function [S1,S2,S3,S4,tt,SNR,Ts] = Hydroacoustic_Signal(R,surface,bottom,depth,f)

%% Source Signal Construction 
omega = f*2*pi;         % Source Frequency rad/s
c = 1500;               % Sound Speed
TS = 1e-6 ;        
SI = 177;               % Source Sound Intensity dB
Ts = 0:TS:0.004;
[~,n] = size(Ts);
tf = (Ts(n)+TS):TS:0.1;
[~,n] = size(tf);
tt = [Ts tf]';
S = [cos(omega*Ts) zeros(1,n)]';      % Signal with baseline magnitude

f = f/1000;

%% Transmission Losses
alpha = (3.3e-3+(0.11*f^2)/(1+f^2)+(44*f^2)/(4100+f^2)+(3e-4)*f^2)/1000; % Attenuation loss (dB/m)

for i = 1:4
    
    if R(i)<5
        TL(i) = 20*log(R(i)) + R(i)*alpha;
    else
        SSL = 20*log(abs(depth));           % Speherical Spreading Losses
        CSL = 10*log(R(i)/abs(depth));      % Cylindrical Spreading Losses
        TL(i) = SSL + CSL + R(i)*alpha;     
        
    end
end

% %% Add noise
NL = 100;                           % Maximum noise level
AG = 0;                             % Array Gain
SNR = SI - TL(1) - (NL - AG);
S = awgn(S,SNR);                 % Add random noise, COMMENT TO REMOVE

%% Hydraphone Signal Construction

for i = 1:4
    P_m(i) =10^((SI-TL(i))/20)*1e-6;     % Calculate Pressure Wave Magnitude in (Pa)
    V_Hyd(i) = P_m(i)*39.81;               % Amplitude of response from hydrophones (Volts)
end

%% Direct Paths
S1 = hydrophone_sig(R(1),tt,c,S,V_Hyd(1));
S2 = hydrophone_sig(R(2),tt,c,S,V_Hyd(2));
S3 = hydrophone_sig(R(3),tt,c,S,V_Hyd(3));
S4 = hydrophone_sig(R(4),tt,c,S,V_Hyd(4));