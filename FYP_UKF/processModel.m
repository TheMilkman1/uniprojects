function [xnext,A,B,Q] = processModel(x,~,param)
%#codegen
Ns = x(1);
Es = x(2);

 xnext =  [Ns;
           Es];   

A = 0;
B = 0;

% Covariance of additive process noise
Q = param.Q;