function [S1mp, S2mp, S3mp, S4mp, tt, H] = SigGen( Cent, psi, hdist,source )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
f = 40e3;           % Source frequency Hz
rho_w = 1023;       % Density of Seawater
rho_g = 2750;       % Density of Bottom Oahu, from
c_g = 1700;         % Speed of Sound for course sand at 40Khz m/s
cw = 1500;          % Speed of Sound in Seawater m/s

depth =-5;

Ro = [  cosd(psi) -sind(psi) 0;  % Yaw rotation matrix
        sind(psi) cosd(psi)  0;
          0          0       1];
      
h1 = (Cent + Ro * [-1.5*hdist 0 0]');
h2 = (Cent + Ro * [-0.5*hdist 0 0]');
h3 = (Cent + Ro * [0.5*hdist 0 0]');
h4 = (Cent + Ro * [1.5*hdist 0 0]');

H = [h1,h2,h3,h4];
%source =[(length*rand(1)),width*rand(1),-1]';%depth*rand(1)]';  % Generate random source location.

%% Signal Construction
[D,B,S,SB,BS,SBS,BSB] = multipath(H,source,depth);                          % Calculate Path lengths
[D_S1,D_S2,D_S3,D_S4,tt,SNR] = Hydroacoustic_Signal(D,0,0,depth,f);             % Direct Path, Source, Reciever
[B_S1,B_S2,B_S3,B_S4,~,~] = Hydroacoustic_Signal(S,0,0,depth,f);            % Source, Bottom, Reciever
[S_S1,S_S2,S_S3,S_S4,~,~] = Hydroacoustic_Signal(B,0,0,depth,f);            % Source, Surface, Reciever
[SB_S1,SB_S2,SB_S3,SB_S4,~,~] = Hydroacoustic_Signal(SB,0,0,depth,f);        % Source, Surface, Bottom, Reciever
[BS_S1,BS_S2,BS_S3,BS_S4,~,~] = Hydroacoustic_Signal(BS,0,0,depth,f);        % Source, Bottom, Surface, Reciever
[SBS_S1,SBS_S2,SBS_S3,SBS_S4,~,~] = Hydroacoustic_Signal(SBS,0,0,depth,f);    % Source, Surface, Bottom, Surface, Reciever
[BSB_S1,BSB_S2,BSB_S3,BSB_S4,~,~] = Hydroacoustic_Signal(BSB,0,0,depth,f);    % Source, Bottom, Surface, Bottom, Reciever

Rrs = -1;                                                                   % Surface Reflection Coefficient from Thompson 2009
Rrb = bottom_abs(H,source,depth,f,c_g,cw,rho_g,rho_w);                      % Bottom Surface Reflection Coefficient 

S1mp = D_S1 + Rrb(1,1)*B_S1 + Rrs*S_S1 + Rrb(1,2)*Rrs*SB_S1 + Rrb(1,3)*Rrs*BS_S1 + Rrb(1,4)*SBS_S1 + Rrb(1,5)*Rrb(1,5)*Rrs*BSB_S1;
S2mp = D_S2 + Rrb(2,1)*B_S2 + Rrs*S_S2 + Rrb(2,2)*Rrs*SB_S2 + Rrb(2,3)*Rrs*BS_S2 + Rrb(2,4)*SBS_S2 + Rrb(2,5)*Rrb(2,5)*Rrs*BSB_S2;
S3mp = D_S3 + Rrb(3,1)*B_S3 + Rrs*S_S3 + Rrb(3,2)*Rrs*SB_S3 + Rrb(3,3)*Rrs*BS_S3 + Rrb(3,4)*SBS_S3 + Rrb(3,5)*Rrb(3,5)*Rrs*BSB_S3;
S4mp = D_S4 + Rrb(4,1)*B_S4 + Rrs*S_S4 + Rrb(4,2)*Rrs*SB_S4 + Rrb(4,3)*Rrs*BS_S4 + Rrb(4,4)*SBS_S4 + Rrb(4,5)*Rrb(4,5)*Rrs*BSB_S4;

end

