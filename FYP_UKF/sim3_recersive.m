% Initilise
clear all
clc
clf
load('pos_n_doa.mat')

%% Parameters
n = length(xo);                   % Number of points on approach
param.t = linspace(0, 100,n-1);
SourceEntry = [10 10 0]';        % Define entry gate
m = 1; % Number of sources

% Estimation Param's
param.Q = diag([1e-40, 1e-40]);
T=1;
param.T = T;
param.R = 0.3;

P_UKF = diag([30,30]); % Initial estimation error covariance

% Initial state with noise
x_UKF = [10;20]; %N E psi u v r

% Init's
x_UKF_hist = zeros(length(x_UKF),length(param.t));
Pdiag_UKF_hist = zeros(length(x_UKF),length(param.t));
y_hist = zeros(m,length(param.t));

%% Estimation loop
 for k = 2:n-1
    % Generate reference stuff
    psi = atan2d(yo(k)-yo(k-1),xo(k)-xo(k-1));  % WAM-V Orientation;
    u = [xo(k); yo(k); psi]; % Input vector - Postion of ownship

    y = Theta_Beam(k);
    
    [x_UKF,P_UKF] = UKF(P_UKF,x_UKF,u,y,@processModel,@measurementModel,param);
    
    x_UKF_hist(:,k) = x_UKF;
    Pdiag_UKF_hist(:,k) = diag(P_UKF);
    y_hist(:,k) = y;
     
 end

%% Plotting

% figure(1)
% plot(t,Theta_Beam,t,Theta_ref)

figure(2)
hold on

XY=.5*[1,  0.9;   1,2; 1.5,3;  2,2;   2,-2; 1,-2;  1,-1.25;
      -1,-1.25; -1,-2; -2,-2; -2,2; -1.5,3; -1,2; -1, .9]; 
param.gates = [10 10 10 10 40;
               5  15 25 35 20]';
bouy_colour = [1   0    0;
               .75 .75 .75;
               .75 .75 .75;
                0   1   0 ;
                0   0   0 ];
k=1;
while k < length(param.t)
    hold off
    plot(xo,yo,':r') %plot trajectory   
    plot(x_UKF_hist(1,k),x_UKF_hist(2,k),'xr') % Plot source position estimate
    %axis([-35 25 -5 45], 'equal')   
    hold on
    cent = [xo(k) yo(k) 0]';  % Centroid of hydrophone array
    psi = atan2d(yo(k+1)-yo(k),xo(k+1)-xo(k));                % WAM-V Orientation;
    
    scatter(SourceEntry(1),SourceEntry(2),'x')
    scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')

    legend('Source', 'trajectory')
    xlabel('Length(m)')
    ylabel('Width(m)')
    title('Competition Field')

    
    plot([xo(k), 60*cosd(psi+Theta_Beam(k))+xo(k) ],[yo(k),60*sind(psi+Theta_Beam(k))+yo(k)],':')
    
    h = patch(XY(:,1) + cent(1),XY(:,2) + cent(2),'blue'); 
    
    rotate(h,[0 0 90],psi-90,cent);
    
    if k>1
    h2 = error_ellipse('C',diag(Pdiag_UKF_hist(:,k)), 'mu',[x_UKF_hist(1,k) x_UKF_hist(2,k)],'conf',.99);
    end
    pause(eps)
    k=k+1;
    %hold off
end
% figure(3)
% hold on
% plot(SourceEntry(1),SourceEntry(2),'rx')
% plot(x_UKF_hist(1,:),x_UKF_hist(2,:),'x')
% hold off

