% Only valid for source deeper than reciever, at this stage

function RrB = bottom_abs(H,source,depth,f,c_g,cw,rho_g,rho_w)

r = sqrt((source(1)-H(1,:)).^2+(source(2)-H(2,:)).^2);

Ds = abs(depth)-abs(source(3));        % Distance from source to bottom
Del = source(3)-H(3,:);

% Calculate for Source, Bottom, Reciever

Opp = 2*Ds+abs(Del);

psi = pi/2 - atan(Opp./r);
gamma1 = f*2*pi*cos(psi)/cw;
gamma2 = (f*2*pi/c_g)*sin(psi).*sqrt((cw^2)./(c_g^2.*sin(psi).^2-1));
B = (rho_g*gamma1-rho_w*gamma2)./(rho_g*gamma1+rho_w*gamma2);

% Calculate for Source, Surface, Bottom, Reciever
Opp = Ds + abs(source(3))+abs(depth);

    
psi = pi/2 - atan(Opp./r);
gamma1 = f*2*pi*cos(psi)/cw;
x = (f*2*pi/c_g);
gamma2 = x*sin(psi).*sqrt((cw^2)./(c_g^2.*sin(psi).^2-1));
SB = (rho_g*gamma1-rho_w*gamma2)./(rho_g*gamma1+rho_w*gamma2);

% Calculate for Source, Bottom, Surface, Reciever
Opp = abs(depth) + 2*abs(source(3))-abs(Del);  % Reflection distance about surface

psi = pi/2 -  atan(Opp./r);
gamma1 = f*2*pi*cos(psi)/cw;
gamma2 = (f*2*pi/c_g)*sin(psi).*sqrt((cw^2)./(c_g^2.*sin(psi).^2-1));
BS = (rho_g*gamma1-rho_w*gamma2)./(rho_g*gamma1+rho_w*gamma2);

% Calculate for Source, Surface, Bottom, Surface, Reciever

Opp = 2*abs(source(3))-abs(Del) + 2*abs(depth) ;                     % Reflection distance about surface

psi =pi/2 - atan(Opp./r);
gamma1 = f*2*pi*cos(psi)/cw;
gamma2 = (f*2*pi/c_g)*sin(psi).*sqrt((cw^2)./(c_g^2.*sin(psi).^2-1));
SBS = (rho_g*gamma1-rho_w*gamma2)./(rho_g*gamma1+rho_w*gamma2);

% Calculate for Source, Bottom, Surface, Bottom, Reciever
Opp =  2*Ds + 2*abs(depth) ;             % Reflection distance about bottom

psi =pi/2 - atan(Opp./r);
gamma1 = f*2*pi*cos(psi)/cw;
gamma2 = (f*2*pi/c_g)*sin(psi).*sqrt((cw^2)./(c_g^2.*sin(psi).^2-1));
BSB = (rho_g*gamma1-rho_w*gamma2)./(rho_g*gamma1+rho_w*gamma2);

RrB = [B' SB' BS' SBS' BSB'];