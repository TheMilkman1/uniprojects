function signal = hydrophone_sig(range,time,C,S0,alpha)

error = 0.000001;
ro = range/C;    % round value value for find function
for i=1:length(time)
    
    if(abs(time(i)-ro)<error)
        shift = i;
    end
    
end

%shift = find((time*1000)==ro);
signal = (circshift(S0,shift))*alpha;
