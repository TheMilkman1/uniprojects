% Initilise
clear all
clc
clf
load('pos_n_doa_works2.mat')

%% Parameters
n = length(xo);                   % Number of points on approach
param.t = linspace(0, 100,n-1);
SourceEntry = [10 10 0]';        % Define entry gate
m = 1; % Number of sources

% Estimation Param's
param.Q = diag([1e-4, 1e-4]);
T=1;
param.T = T;
param.R = 0.3;

P_UKF = diag([10,30]); % Initial estimation error covariance

% Initial state with noise
x_UKF = [10;20]; %N E 

% Init's
x_UKF_hist = zeros(length(x_UKF),length(param.t));
Pdiag_UKF_hist = zeros(length(x_UKF),length(param.t));
y_hist = zeros(m,length(param.t));

%% Estimation loop
 for k = 2:n-1
    % Generate reference stuff
    psi = atan2d(yo(k)-yo(k-1),xo(k)-xo(k-1));  % WAM-V Orientation;
    u = [xo(k); yo(k); psi]; % Input vector - Postion of ownship

    y = Theta_Beam(k);
    
    [x_UKF,P_UKF] = UKF(P_UKF,x_UKF,u,y,@processModel,@measurementModel,param);
    
    x_UKF_hist(:,k) = x_UKF;
    Pdiag_UKF_hist(:,k) = diag(P_UKF);
    y_hist(:,k) = y;
     
 end

%% Plotting
figure(2)
hold on

XY=.7*[1,  0.9;   1,2; 1.5,3;  2,2;   2,-2; 1,-2;  1,-1.25;
      -1,-1.25; -1,-2; -2,-2; -2,2; -1.5,3; -1,2; -1, .9]; 
param.gates = [10 10 10 10 40;
               5  15 25 35 20]';
bouy_colour = [1   0    0;
               .75 .75 .75;
               .75 .75 .75;
                0   1   0 ;
                0   0   0 ];
k=2;
plot(xo,yo,':r') %plot trajectory 
while k < length(param.t)
    hold off

    if k>2
        plot(x_UKF_hist(1,k),x_UKF_hist(2,k),'xr') % Plot source position estimate
    end  
     
    if k < 3
         plot(10,20,'xr')
    end
    
    axis([-25 42 3 37])
    hold on
    scatter(SourceEntry(1),SourceEntry(2),'om')
    cent = [xo(k) yo(k) 0]';  % Centroid of hydrophone array
    psi = atan2d(yo(k+1)-yo(k),xo(k+1)-xo(k));                % WAM-V Orientation;
    plot([xo(k), 40*cosd(psi+Theta_Beam(k))+xo(k) ],[yo(k),40*sind(psi+Theta_Beam(k))+yo(k)],':b')
    plot([xo(k), 20*cosd(psi-Theta_Beam(k))+xo(k) ],[yo(k),20*sind(psi-Theta_Beam(k))+yo(k)],':c')
    
    scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')
    
    %legend('Estimated Source Location', 'Source Location')
    legend('Estimated Source Location','Source Location', 'Port Bearing Estimate','Starboard Bearing Estimate')
    xlabel('Easting (m)')
    ylabel('Northing (m)')
    title('Competition Field')
    if k < 3
        h2 = error_ellipse('C',[10 0; 0 30], 'mu',[10 20],'conf',0.99);
    end
    h = patch(XY(:,1) + cent(1),XY(:,2) + cent(2),'blue'); 
    
    rotate(h,[0 0 90],psi-90,cent);
    
    if k>2
    h2 = error_ellipse('C',diag(Pdiag_UKF_hist(:,k)), 'mu',[x_UKF_hist(1,k) x_UKF_hist(2,k)],'conf',0.99);
    end
    pause(eps)
    saveas(gcf,'ukf.png')
    k=k+1;
end