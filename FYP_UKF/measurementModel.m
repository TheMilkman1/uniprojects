function [y,C,D,R] = measurementModel(x,u,param)

No = u(1);
Eo = u(2);
psi = u(3);


Ns = x(1); 
Es = x(2);


y = atan2d(Es-Eo,Ns-No)-psi;

C = 0;
D = 0;
% Covariance of additive measurement noise
R = param.R*eye(1);