% Sequential Importance Particle Filter V3
% Douglas Byrne
% FYP 2016

% More robust, more samples on approach
% Utilises ESS criterion
% pinger-based transit simulation.


%% Initilise
clear all
clc
clf
load('pos_n_doa3.mat')

n = length(xo);                   % Number of points on approach
t = linspace(0, 100,n-1);

%% Parameters
SourceEntry = [10 10 0]';        % Define entry gate - What we're localising

% Plotting Params
XY=.5*[1,  0.9;   1,2; 1.5,3;  2,2;   2,-2; 1,-2;  1,-1.25;
      -1,-1.25; -1,-2; -2,-2; -2,2; -1.5,3; -1,2; -1, .9]; 
param.gates = [10 10 10 10 40;
               5  15 25 35 20]';
bouy_colour = [1   0    0;
               .75 .75 .75;
               .75 .75 .75;
                0   1   0 ;
                0   0   0 ];

m = 2;              % Number of states
Q = 0.02*eye(m);   % Process Noise
R = 1^2;            % Measurement covarience
M   = 10^4;         % Number of particles
z0 = [-40 -20];
p0 = [100 70]; 
% Initial particle distribution
xpp = [z0(1)+p0(1)*(rand(1,M));z0(2)+p0(2)*rand(1,M)];
psi = zeros(1,n-1); % Init. psi

for t=2:n-1    
    psi = atan2d(yo(t)-yo(t-1),xo(t)-xo(t-1)); % Calculates WAM-V yaw
    yp1 = atan2d(xpp(2,:)-yo(t),xpp(1,:)-xo(t)) - psi;  % Measurements for every particle
    
%     figure(2)
%     ksdensity(yp1)
%     title('Measurement or State Likelihood')
%     xlabel('Direction of Arrival (�)')
%     ylabel('Probability Density(~)')
%     axis([-100 100 0 .45])
%     saveas(gcf,'pf_pdf.png')
    
    ymeasured = Theta_Beam(t);

    py = 1/sqrt(2*pi*R)*exp(-(.5/R)*(ymeasured - yp1).^2) + 1/sqrt(2*pi*R)*exp(-(.5/R)*(ymeasured + yp1).^2); % measurement likelyhood
    
    w = py/sum(py); % Normalise the weights
    
    % Effective sample size criterion 
    ESS = 1/sum(w.^2);
    if ESS < M*.5
        ii = randsample(M,M,true,w); % gives you an index of the particle (i)
        xpp = xpp(:,ii); % states don't change, your xpp = xpp old. Random Walk
    end
    
    % Random walk model
    xpp = xpp + sqrtm(Q)*randn(m,M); %move particles around to test other local positions
    
    % Averages particle location to find actual estimated position
    Pos_est = [sum(xpp(1,:))/length(xpp)  sum(xpp(2,:))/length(xpp)];
    
    % Plostting stuff
    figure(1)
    clc;
    cent = [xo(t) yo(t) 0]';  % Centroid of hydrophone array               % WAM-V Orientation;
    
    plot(xo,yo,':r') %plot trajectory   
    hold on
    scatter(xpp(1,:),xpp(2,:),'y')
    plot(Pos_est(1),Pos_est(2),'xb','MarkerSize',10)
    
    scatter(param.gates(:,1),param.gates(:,2),10^2,bouy_colour,'filled')
    scatter(SourceEntry(1),SourceEntry(2),'rx')
    
    % Plots dashed lines representing estimated bearings to the source
    plot([xo(t), 60*cosd(psi+ymeasured)+xo(t) ],[yo(t),60*sind(psi+ymeasured)+yo(t)],'--')
    plot([xo(t), 60*cosd(psi-ymeasured)+xo(t) ],[yo(t),60*sind(psi-ymeasured)+yo(t)],'--')
    
    legend('Trajectory','Particles','Estimated Position','Source', 'Possible Bearing From Boat','Location','northeast')
    xlabel('Length(m)')
    ylabel('Width(m)')
    title('Competition Field')
    
    % Boat patch/picture
    h = patch(XY(:,1) + cent(1),XY(:,2) + cent(2),'blue'); 
    rotate(h,[0 0 90],psi-90,cent); 
    

    hold off
    axis([-30 50 0 40])
    saveas(gcf,'pf_rob.png')
    pause(eps)
end

